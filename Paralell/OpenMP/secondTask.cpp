//
// Created by Артем Кондратьев on 10.10.2021.
//

#include <iostream>
#include <omp.h>

int main()
{
    int thr = 3;

#pragma omp parallel if(thr > 1) num_threads(thr)
    {
        int num = omp_get_thread_num();
        int thnum = omp_get_num_threads();
        printf("Number of threads in first - %d, Number of thread - %d\n", thnum, num);
    }
    thr = 1;
#pragma omp parallel if(thr > 1) num_threads(thr)
    {
        int num = omp_get_thread_num();
        int thnum = omp_get_num_threads();
        printf("Number of threads in second - %d, Number of thread - %d\n", thnum, num);
    }

}