//
// Created by Артем Кондратьев on 10.10.2021.
//

#include <iostream>
#include <omp.h>

int main()
{
    int a;
    int b;
    a = 8;
    b = 12;
    printf("Before first parallel a=%d, b=%d\n", a, b);
#pragma omp parallel private(a) firstprivate(b) num_threads(2)
    {
        a = 8;
        int step = omp_get_thread_num();
        a += step;
        b += step;
        printf("Inside first parallel a=%d,b=%d\n", a, b);
    }
    printf("After first parallel a=%d,b=%d\n", a, b);
    printf("Before second parallel a=%d, b=%d\n", a, b);
#pragma omp parallel num_threads(4) shared(a) private(b)
    {
        b = 12;
        int step = omp_get_thread_num();
        a += step;
        b += step;
        printf("Inside second parallel a=%d,b=%d\n", a, b);
    }
    printf("After second parallel a=%d,b=%d\n", a, b);

}