#include <omp.h>
#include <stdio.h>
#include <algorithm>
#include <Windows.h>
#include <ctime>
#include <chrono>
using namespace std;
int main()
{
    omp_set_num_threads(8);
    printf("Method 1\n");
#pragma omp parallel
    {
        Sleep((omp_get_num_threads() - omp_get_thread_num()) * 1000);
        printf("This is thread %1d/%1d, Hello world\n", omp_get_thread_num(), omp_get_num_threads());
    }
    printf("Method 2\n");
#pragma omp parallel
    {
        auto start = chrono::system_clock::now();
        auto now = chrono::system_clock::now();
        chrono::duration<double> elapsed = now - start;
        while (elapsed.count() < omp_get_num_threads() - omp_get_thread_num())
        {
            now = chrono::system_clock::now();
            elapsed = now - start;
        }
        printf("This is thread %1d/%1d, Hello world\n", omp_get_thread_num(), omp_get_num_threads());
    }
}