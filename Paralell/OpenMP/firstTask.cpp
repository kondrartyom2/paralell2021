//
// Created by Артем Кондратьев on 10.10.2021.
//
#include <omp.h>
#include <stdio.h>

int main()
{
    omp_set_num_threads(8);
#pragma omp parallel
    {
        printf("Thread - %1d/%1d, Hello world\n", omp_get_thread_num(), omp_get_num_threads());
    }
}