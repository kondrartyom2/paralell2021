//
// Created by Артем Кондратьев on 10.10.2021.
//

#include <iostream>
#include <omp.h>

int main()
{
    int d[6][8];

    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 8; j++) {
            d[i][j] = rand();
            srand(rand() + 4);
        }
    }
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 8; j++) {
            printf("%d ", d[i][j]);
        }
        printf("\n");
    }

#pragma omp parallel shared(d)
    {
#pragma omp sections
        {
#pragma omp section
            {
                float k = 0;
                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 8; j++) {
                        k += d[i][j];
                    }
                }
                float avg = k / 48;
                int n = omp_get_thread_num();
                printf("Average - %g, Thread - %d\n", avg, n);
            }
#pragma omp section
            {
                int max = INT_MIN;
                int min = INT_MAX;
                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (d[i][j] > max) {
                            max = d[i][j];
                        }
                        if (d[i][j] < min) {
                            min = d[i][j];
                        }
                    }
                }
                int n = omp_get_thread_num();
                printf("max - %d, min - %d, thread - %d\n", max, min, n);
            }
#pragma omp section
            {
                int k = 0;
                for (int i = 0; i < 6; i++) {
                    for (int j = 0; j < 8; j++) {
                        if (d[i][j] % 3 == 0) {
                            k += 1;
                        }
                    }
                }
                int n = omp_get_thread_num();
                printf("Separated by 3 - %d ,Thread - %d\n", k, n);
            }
        }
    }

}