#include <stdio.h>
#include "mpi.h"
#include <iostream>
#include <algorithm>
using namespace std;



int main(int argc, char** argv)
{
	int m = 6;
	int n = 6;
	int size, rank;
	int len = m * n;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank == 0)
	{
		double* a = new double[len];
		for (int i = 0; i < len; i++)
		{
			a[i] = rand() % 10;
		}
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				printf("%.2f ", a[i * n + j]);
			}
			printf("\n");
		}
		printf("----------------------\n");
		int block = (m / (size - 1) + 1) * n;
		for (int i = 1; i < size; i++)
		{
			int start = (i - 1) * block;
			int end = i * block;
			if (start > len)
			{
				start = len;
			}
			if (end > len)
			{
				end = len;
			}
			if (start == end)
			{
				continue;
			}
			int count = end - start;
			double* send = new double[count];
			for (int j = start; j < end; j++)
			{
				send[j - start] = a[j];
			}
			MPI_Send(send, count, MPI_DOUBLE, i, 2, MPI_COMM_WORLD);
		}
		// Now we have array parts on each proccess

		bool* completedRows = new bool[m];
		for (int row = 0; row < m; row++)
		{
			completedRows[row] = false;
		}
		for (int iteration = 0; iteration < m; iteration++)
		{
			int index = -1;
			for (int i = 0; i < m; i++)
			{
				if (a[i * n + iteration] != 0 and !completedRows[i])
				{
					index = i;
					completedRows[i] = true;
					break;
				}
			}
			for (int i = 1; i < size; i++)
			{
				int start = (i - 1) * block;
				int end = i * block;
				if (start > len)
				{
					start = len;
				}
				if (end > len)
				{
					end = len;
				}
				if (start == end)
				{
					continue;
				}
				MPI_Send(&index, 1, MPI_INT, i, 10 * iteration + 5, MPI_COMM_WORLD);
			}
			if (index == -1)
			{
				continue;
			}
			double* sendrow = new double[n];
			for (int i = 0; i < n; i++)
			{
				sendrow[i] = a[index * n + i];
			}
			for (int i = 1; i < size; i++)
			{
				int start = (i - 1) * block;
				int end = i * block;
				if (start > len)
				{
					start = len;
				}
				if (end > len)
				{
					end = len;
				}
				if (start == end)
				{
					continue;
				}
				MPI_Send(sendrow, n, MPI_DOUBLE, i, 10 * (iteration + 1), MPI_COMM_WORLD);
			}
			for (int i = 1; i < size; i++)
			{
				int start = (i - 1) * block;
				int end = i * block;
				if (start > len)
				{
					start = len;
				}
				if (end > len)
				{
					end = len;
				}
				if (start == end)
				{
					continue;
				}
				int count = end - start;
				double* recv = new double[count];
				MPI_Status status;
				MPI_Recv(recv, count, MPI_DOUBLE, i, 10 * (iteration + 1) + 1, MPI_COMM_WORLD, &status);
				for (int j = 0; j < count; j++)
				{
					a[start + j] = recv[j];
				}
			}
		}
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				printf("%.2f ", a[i * n + j]);
			}
			printf("\n");
		}
	}
	else
	{
		int block = (m / (size - 1) + 1) * n;
		int start = (rank - 1) * block;
		int end = rank * block;
		if (start > len)
		{
			start = len;
		}
		if (end > len)
		{
			end = len;
		}
		if (start == end)
		{
			MPI_Finalize();
			return 0;
		}
		int count = end - start;
		double* recv = new double[count];
		MPI_Status status;
		MPI_Recv(recv, count, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD, &status);
		for (int iteration = 0; iteration < m; iteration++)
		{
			int index;
			MPI_Status status;
			MPI_Recv(&index, 1, MPI_INT, 0, 10 * iteration + 5, MPI_COMM_WORLD, &status);
			if (index == -1)
			{
				continue;
			}
			else
			{
				double* row = new double[n];
				MPI_Recv(row, n, MPI_DOUBLE, 0, 10 * (iteration + 1), MPI_COMM_WORLD, &status);
				for (int i = start / n; i < end / n; i++)
				{
					if (index == i)
					{
						continue;
					}
					for (int j = iteration + 1; j < n; j++)
					{
						recv[(i - start / n) * n + j] -= recv[(i - start / n) * n + iteration] / row[iteration] * row[j];
					}
					recv[(i - start / n) * n + iteration] = 0;
				}
			}
			
			MPI_Send(recv, count, MPI_DOUBLE, 0, 10 * (iteration + 1) + 1, MPI_COMM_WORLD);
		}
		
	}
	MPI_Finalize();
	return 0;
}