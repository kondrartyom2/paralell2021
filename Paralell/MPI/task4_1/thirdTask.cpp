#include <stdio.h>
#include "mpi.h"
#include <iostream>
#include <chrono>
using namespace std;



int main(int argc, char** argv)
{
	int n = 10000;
	int size, rank;


	auto start = chrono::system_clock::now();

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	if (rank == 0)
	{
		int* a = new int[n];
		for (int i = 0; i < n; i++)
		{
			a[i] = i;
		}

		for (int recv = 1; recv < size; recv++)
		{
			MPI_Status status;
			for (int i = 0; i < n; i++)
			{
				MPI_Send(a, n, MPI_INT, recv, 1, MPI_COMM_WORLD);
				int* buf = new int[n];
				MPI_Recv(buf, n, MPI_INT, recv, 2, MPI_COMM_WORLD, &status);
			}
		}
	}
	else
	{
		MPI_Status status;
		for (int i = 0; i < n; i++)
		{
			int* buf = new int[n];
			MPI_Recv(buf, n, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
			MPI_Send(buf, n, MPI_INT, 0, 2, MPI_COMM_WORLD);
		}
	}

	auto now = chrono::system_clock::now();
	chrono::duration<double> elapsed = now - start;

	if (rank == 0) printf("Blocking send-receive lasted for %f s. Now starting asynchronious\n", elapsed.count());

	auto start2 = chrono::system_clock::now();
	MPI_Request request;
	MPI_Status status;
	int* a = new int[n];

	for (int i = 0; i < n; i++)
	{
		a[i] = i;
	}

	if (rank == 0)
	{
		for (int i = 0; i < n; i++)
		{
			MPI_Isend(a, n, MPI_INT, 1, 1, MPI_COMM_WORLD, &request);
			MPI_Wait(&request, &status);
			MPI_Irecv(a, n, MPI_INT, 1, 2, MPI_COMM_WORLD, &request);
			MPI_Wait(&request, &status);
		}
	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			MPI_Irecv(a, n, MPI_INT, 0, 1, MPI_COMM_WORLD, &request);
			MPI_Wait(&request, &status);
			MPI_Isend(a, n, MPI_INT, 0, 2, MPI_COMM_WORLD, &request);
			MPI_Wait(&request, &status);
		}
	}
	auto now2 = chrono::system_clock::now();
	chrono::duration<double> elapsed2 = now2 - start2;

	if (rank == 0) printf("Non-blocking send-receive lasted for %f s. Finishing\n", elapsed2.count());


	MPI_Finalize();
	return 0;
}