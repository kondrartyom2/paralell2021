#include <stdio.h>
#include "mpi.h"
#include <iostream>
#include <algorithm>
using namespace std;



int main(int argc, char** argv)
{
	int n = 12;
	int size, rank;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	if (rank == 0)
	{
		int* a = new int[n];
		for (int i = 0; i < n; i++)
		{
			a[i] = rand() % 10;
		}
		int block = (n / size) + 1;
		for (int i = 1; i < size; i++)
		{
			int start = block * (i - 1);
			int end = block * i;
			int count = end - start;
			int* arr = new int[count];
			for (int i = 0; i < count; i++)
			{
				arr[i] = a[i + start];
			}
			printf("0. Sending array of length %d to procces %d with next elements:\n", count, i);
			for (int i = 0; i < count; i++)
			{
				printf("%d ", arr[i]);
			}
			printf("\n");
			
			MPI_Send(arr, count, MPI_INT, i, 5, MPI_COMM_WORLD);

		}
	}
	else
	{
		int count;
		MPI_Status status;
		MPI_Probe(0, 5, MPI_COMM_WORLD, &status);
		MPI_Get_count(&status, MPI_INT, &count);

		int* arr = new int[count];
		MPI_Recv(arr, count, MPI_INT, 0, 5, MPI_COMM_WORLD, &status);
		printf("%d. Recieved an array of length %d from process %d with next elements:\n", rank, count, 0);
		for (int i = 0; i < count; i++)
		{
			printf("%d ", arr[i]);
		}
		printf("\n");
	}
	MPI_Finalize();
	return 0;
}