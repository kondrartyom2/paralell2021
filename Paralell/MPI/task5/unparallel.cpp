//
// Created by Артем Кондратьев on 28.11.2021.
//

#include <stdio.h>
#include <iostream>
#include <random>
#include <time.h>
#include <chrono>
using namespace std;


double fRand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

int main(int argc, char** argv)
{
	int iterations = 2500000 * 4;
	int count = 0;
	srand(time(0));
	auto start = chrono::system_clock::now();
	
	for (int i = 0; i < iterations; i++)
	{
		double x = fRand(-1, 1);
		double y = fRand(-1, 1);
		if (x * x + y * y <= 1)
		{
			count += 1;
		}
	}
	auto now = chrono::system_clock::now();
	chrono::duration<double> elapsed = now - start;
	printf("pi/4 = %f\n", double(count) / iterations * 4);
	printf("real - %.4f\n", elapsed.count());
	return 0;
}